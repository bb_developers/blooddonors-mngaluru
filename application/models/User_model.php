<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User_model extends CI_Model  {



		 public function construct()   
          {
	       parent::_construct();
	      $this->load->database();
	      }
	
	
	 public function create_user($userName, $password)  
	     {
	        	$data= array(
							
			               'username'   =>  $userName,
			               'password'   => $this->hash_password($password),
			               'type'      => '2',
						   'created_at'	=> date('Y-m-j H:i:s'),
						   'status' =>1);
		return $this->db->insert('user', $data);
			 
		}
	
	
		public function user_login($username, $password)
		      {
			  $this->db->select('password');
			  $this->db->from('user');
			  $this->db->where('username',$username );
			   $this->db->where('status=','1' );
					  
			  $hash=$this->db->get()->row('password');
			  
			  return $this->verify_password_hash($password, $hash);
			  
			  }
			  
			  private function hash_password($password) {
		
		         return password_hash($password, PASSWORD_BCRYPT);
		
	         }
	        public function verify_password_hash($password, $hash)
		    {
			
			 return password_verify($password, $hash);
			  			  
			}
			public function get_user_id_from_username($username)
			   {
			     $this->db->select('user_id');
				 $this->db->from('user');
				 $this->db->where('username', $username);
				 return $this->db->get()->row('user_id');
				 
			  }
			 public function get_user($user_id)
			   {
			    $this->db->from('user');
				$this->db->where('user_id', $user_id);
				return $this->db->get()->row();
				
			   } 
			 public function get_status($user_id)
			    {
				$this->db->select('status');
				$this->db->from('user');
				$this->db->where('user_id', $user_id);
				 return $this->db->get()->row('status');
				
				}  
			   
			public function get_profiles()
			   {
			     $this->db->select('*');
				 $this->db->from('user');
				 $query=$this->db->get();
		         return $query->result();
			   } 
		   public function get_profile($action_id)
			   {
			     $this->db->select('*');
				 $this->db->from('user');
				 $this->db->where('user_id', $action_id);
				 $query=$this->db->get();
		         return $query->result();
			   }  
		  public function delete_profile($action_id)
		      {
			    $this->db->where('user_id', $action_id);
				$this->db->where('status !=', '3');
                $this->db->delete('user');
			    return $action_id;
			  }	   
			   
}