<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Client_model extends CI_Model  {

		 public function construct()   
          {
	       parent::_construct();
	      $this->load->database();
	      }
		  
	      public function save_client($clientName, $clientContact, $clientbudget, $client_payment, $address)
          {
			   $data=array('client_name'=>$clientName,
			               'client_contact'=>$clientContact,
			                'client_address'=>$address,
							'Deal_amount'=>$clientbudget,
							 'transaction_amount'=>$client_payment,
    	                    'created_at'=>date('Y-m-j H:i:s'),	
			                'status'=>1);
			      return $this->db->insert('client', $data);
			    
		 }
		public function get_active_clients()
		{
		    $this->db->select('*');
		    $this->db->from('client');
			$this->db->where('status', 1);
			$query=$this->db->get();
		    return $query->result();
	    }  		
		public function get_client_details($id)
		{
		    $this->db->select('*');
		    $this->db->from('client');
			$this->db->where('client_id', $id);
			$query=$this->db->get();
		    return $query->result();
		}
		public function update_client($clientName, $clientContact, $clientbudget,$client_payment, $address, $id)
		{
		$data=array('client_name'=>$clientName,
			               'client_contact'=>$clientContact,
			                'client_address'=>$address,
							'Deal_amount'=>$clientbudget,
							'transaction_amount'=>$client_payment);
							$this->db->where('client_id', $id);
			      return $this->db->update('client', $data);
		}
	   public function delete_client_details($id)
	    {
		 $this->db->where('client_id', $id);
          return $this->db->delete('client');
		
		}
		public function get_transaction_details()
		 {
		   $this->db->select('*');
		   $this->db->from('client_transaction');
		   $query=$this->db->get();
		   return $query->result();
		
		 }
		
/* ***************** ********************************Manage_contract_employe***************************************************************** */		
		public function add_employe_details($data)
		{
		    
		   return $this->db->insert('employe', $data);
		
		}
		public function get_active_employes()
		{
		    $this->db->select('*');
		    $this->db->from('employe');
			$this->db->where('status', 1);
			$query=$this->db->get();
		    return $query->result();
		}
	public function get_employe_details($id)
		{
		    $this->db->select('*');
		    $this->db->from('employe');
			$this->db->where('employe_id', $id);
			$query=$this->db->get();
		    return $query->result();
		}
	public function update_employe($data, $id)
	{
	   $this->db->where('employe_id', $id);
		return $this->db->update('employe', $data);
	
	}
	
	public function delete_employe_details($id)
	    {
		 $this->db->where('employe_id', $id);
          return $this->db->delete('employe');
		
		}
	public function get_active_client()
	 {
	        $this->db->select('client_id,client_name');
		    $this->db->from('client');
			$this->db->where('status', 1);
			$query=$this->db->get();
		    return $query->result();
	 }	
	 public function Get_active_employe()
	 {
	        $this->db->select('*');
		    $this->db->from('employe');
			$this->db->where('status', 1);
			$query=$this->db->get();
		    return $query->result();
	 }	
	 public function Add_transaction_process($data, $amount, $id)
	 {
	   if($this->db->insert('client_transaction',$data)){
	         
			  $this->db->select('transaction_amount');
		      $this->db->from('client');
			   $this->db->where('client_id', $id);
			    $query=$this->db->get()->row('transaction_amount');
				$newvalue=$query-$amount;
	            $this->db->where('client_id', $id);
		        $this->db->set('transaction_amount',$newvalue);
	            return $this->db->update('client');

	   }	 
	 }
	 public function edit_transaction_details($id)
		{
		    $this->db->select('*');
		    $this->db->from('client_transaction');
			$this->db->where('client_transaction_id', $id);
			$query=$this->db->get();
		    return $query->result();
		}
		
	  public function update_transaction_process($data, $amount, $id, $Tr_id, $old_amount)
	 {    $this->db->where('client_transaction_id', $Tr_id);
	   if($this->db->update('client_transaction',$data)){
	         
			  $this->db->select('transaction_amount');
		      $this->db->from('client');
			   $this->db->where('client_id', $id);
			    $query=$this->db->get()->row('transaction_amount');
				$value=$query+$old_amount;
				$newvalue=$value-$amount;
	            $this->db->where('client_id', $id);
		        $this->db->set('transaction_amount',$newvalue);
	            return $this->db->update('client');

	   }	 
	 }
	   public function delete_transaction_details($id)
	    {
		   $this->db->where('client_transaction_id', $id);
          return $this->db->delete('client_transaction');

		}
			   
}