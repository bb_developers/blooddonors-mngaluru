<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class admin_model extends CI_Model  {
	
	//declaration
     private $Mytablel;

     	 public function construct()   
          {
	       parent::_construct();
	      $this->load->database();
	      }
		  
		   public function getTableData($table, $id)
	       { 		
	         $this->db->select('*');
		     $this->db->from($table);
			 if(count($id) > 0){
				foreach($id as $key=>$value){
				  $this->db->where('`'.$key.'`', $value);
				}
			 }
			
		 	 $query = $this->db->get();  			  
             return $query->result();
		   }
		   
		   public function updateDonorDetail($id, $data)
		   {
			  $this->db->where('Donor_Id', $id);
              $this->db->update('donor_registration', $data);
			  return true;
		   }
		   
		   public function insertTable($table, $data)
		   {
			   $query=$this->db->insert($table, $data);
			   if($query)
		       {
		        return true;
		       }
		       else
		       {
		        return false;
		       }
		   }
		   
		   public function updateFile($id, $data)
		   {
			  $this->db->where('file_upload_id', $id);
              $this->db->update('file_upload', $data);
			  return true;
		   }		   
		   
}