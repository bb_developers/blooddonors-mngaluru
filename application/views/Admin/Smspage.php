 <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>ADD NEWS</h2>
		    </div>
			 <!-- Textarea -->
			 <!-- File Upload | Drag & Drop OR With Click & Choose -->
           
                   
        <?=form_open(base_url().'Dashboard/server_sending'); ?>
			 <!-- Basic Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>NEWS INFORMATION</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="title" required>
                                        <label class="form-label">Title</label>
                                    </div>
                                </div>
                                
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea name="description" cols="30" rows="5" class="form-control no-resize" required></textarea>
                                        <label class="form-label">Description</label>
                                    </div>
                                </div>
                               <button class="btn btn-block btn-lg btn-primary waves-effect" data-type="success" type="submit" id="submit_dropzone_form">Send</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Validation -->
			
          
        </div>
 </section>
 <script>
Dropzone.options.myAwesomeDropzone = {
	autoProcessQueue: false,
  	uploadMultiple: true,
  	parallelUploads:10,
	successmultiple:function(data,response){
		$("#uploaded_files").val(response);
	},
	init: function() {
		//Submitting the form on button click
		var submitButton = document.querySelector("#submit_dropzone_form");
			myDropzone = this; // closure
			submitButton.addEventListener("click", function() {
			myDropzone.processQueue(); // Tell Dropzone to process all queued files.
		});
	}
};
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/Admin/plugins/dropzone/dropzone.js"></script>
    <script src="<?php echo base_url(); ?>assets/Admin/js/pages/ui/dialogs.js"></script>


	