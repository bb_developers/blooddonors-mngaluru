<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    NEWS DATA
                </h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               LIST OF DETAILS
							 </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Title</th>
											<th>News</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Title</th>
											<th>News</th>
                                            <th>Date</th>
                                           
                                        </tr>
                                    </tfoot>
                                    <tbody>
									<?php foreach($news as $key) : ?>
                                        <tr class="<?=$key->news_id?>">
										<td><?=$key->news_id?></td>
                                        <td><a  data-toggle="tooltip" data-placement="top" title="<?=$key->description?>"><?=$key->title?></a></td>
                                         <td> <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                                                <button type="button" class="btn btn-primary  waves-effect" data-trigger="focus" data-container="body"                                                      data-toggle="popover"  data-placement="top" title="News" data-content="<?=$key->description?>.">                                                         View</button>
                                              </div>
								         </td>										
										 <td><?= $key->dates ?></td>
                                        </tr>
                                       <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples --> </div>
        </div>
 