
    <!-- Jquery Core Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/raphael/raphael.min.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="<?= base_url();?>assets/Admin/plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/flot-charts/jquery.flot.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="<?= base_url();?>assets/Admin/js/admin.js"></script>
    <script src="<?= base_url();?>assets/Admin/js/pages/index.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
	<script src="<?= base_url();?>assets/Admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

 <!-- Dropzone Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/dropzone/dropzone.js"></script>

    <!-- Input Mask Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

    <!-- Multi Select Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/multi-select/js/jquery.multi-select.js"></script>

    <!-- Jquery Spinner Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-spinner/js/jquery.spinner.js"></script>

 <!-- Bootstrap Tags Input Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

 <!-- noUISlider Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/nouislider/nouislider.js"></script>

    <!-- Custom Js -->
    <script src="<?= base_url();?>assets/Admin/js/pages/tables/jquery-datatable.js"></script>

    <script src="<?= base_url();?>assets/Admin/js/pages/forms/advanced-form-elements.js"></script>
	
	
    <!-- JQuery Steps Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-steps/jquery.steps.js"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Waves Effect Plugin Js -->

    <!-- Custom Js -->
    <script src="<?= base_url();?>assets/Admin/js/pages/forms/form-wizard.js"></script>


    <!-- Demo Js -->
    <script src="<?= base_url();?>assets/Admin/js/demo.js"></script>
    <script src="<?= base_url();?>assets/Admin/js/pages/forms/form-validation.js"></script>
	<script src="<?= base_url();?>assets/Admin/plugins/jquery-validation/jquery.validate.js"></script>


</body>

</html>