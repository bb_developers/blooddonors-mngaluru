 <!-- Jquery Core Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-validation/jquery.validate.js"></script>

		<!-- JQuery Steps Plugin Js -->
		<script src="<?= base_url();?>assets/Admin/plugins/jquery-steps/jquery.steps.js"></script>

		<!-- Sweet Alert Plugin Js -->
		<script src="<?= base_url();?>assets/Admin/plugins/sweetalert/sweetalert.min.js"></script>

		<!-- Waves Effect Plugin Js -->
		<script src="<?= base_url();?>assets/Admin/plugins/node-waves/waves.js"></script>

		<!-- Custom Js -->
		<script src="<?= base_url();?>assets/Admin/js/admin.js"></script>
		<script src="<?= base_url();?>assets/Admin/js/pages/forms/form-wizard.js"></script>

    <!-- Demo Js -->
    <script src="<?= base_url();?>assets/Admin/js/demo.js"></script>
	    <script src="<?= base_url();?>assets/Admin/plugins/multi-select/js/jquery.multi-select.js"></script>
    <script src="<?= base_url();?>assets/Admin/js/pages/forms/advanced-form-elements.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/dropzone/dropzone.js"></script>

</body>
</html>