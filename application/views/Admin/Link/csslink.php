<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>BDM- Admin panel</title>
    <!-- Favicon-->
    <link rel="icon" href="<?= base_url();?>assets/Admin/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= base_url();?>assets/Admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= base_url();?>assets/Admin/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= base_url();?>assets/Admin/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="<?= base_url();?>assets/Admin/plugins/morrisjs/morris.css" rel="stylesheet" />

	  <!-- JQuery DataTable Css -->
    <link href="<?= base_url();?>assets/Admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
     
	<link href="<?= base_url();?>assets/Admin/plugins/dropzone/dropzone.css" rel="stylesheet">
       
    <link href="<?= base_url();?>assets/Admin/plugins/multi-select/css/multi-select.css" rel="stylesheet">

<!-- Bootstrap Spinner Css -->
    <link href="<?= base_url();?>assets/Admin/plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet">

    <!-- Bootstrap Tagsinput Css -->
    <link href="<?= base_url();?>assets/Admin/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="<?= base_url();?>assets/Admin/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- noUISlider Css -->
    <link href="<?= base_url();?>assets/Admin/plugins/nouislider/nouislider.min.css" rel="stylesheet" />

    <link href="<?= base_url();?>assets/Admin/plugins/sweetalert/sweetalert.css" rel="stylesheet" />

   
    <!-- Custom Css -->
    <link href="<?= base_url();?>assets/Admin/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?= base_url();?>assets/Admin/css/themes/all-themes.css" rel="stylesheet" />
</head>


