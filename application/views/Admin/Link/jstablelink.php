<!-- Jquery Core Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?= base_url();?>assets/Admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="<?= base_url();?>assets/Admin/js/admin.js"></script>
    <script src="<?= base_url();?>assets/Admin/js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <script src="<?= base_url();?>assets/Admin/js/demo.js"></script>
	<script src="<?= base_url();?>assets/Admin/js/pages/ui/tooltips-popovers.js"></script>

</body>

</html>