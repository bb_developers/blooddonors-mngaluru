<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->helper(array('form', 'url'));
        }

        public function index()
        {
			$this->load->model('admin_model');
			$this->load->view('Admin/Link/csslink');
			$this->load->view('Admin/sidebar');		
			$this->load->view('Admin/upload_image', array('error' => ' ' ));
			$this->load->view('Admin/footer');		   
			$this->load->view('Admin/Link/jslink');
        }

      /*  public function do_upload()
        {
			$config['upload_path']          = './assets/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['max_size']             = 100;
			$config['max_width']            = 1024;
			$config['max_height']           = 768;

			$this->load->library('upload', $config);
			//$filename = time().'_'.$this->input->post('name');
			print_r($this->input->post('upload_image')); die();
			if ( ! $this->upload->do_upload('upload_image'))
			{
				$msg = array('error' => $this->upload->display_errors());
				$this->load->model('admin_model');
				$this->load->view('Admin/Link/csslink');
				$this->load->view('Admin/sidebar');	
				$this->load->view('Admin/upload_image', $msg);
				$this->load->view('Admin/footer');		   
				$this->load->view('Admin/Link/jslink');
			}
			else
			{
				$this->load->model('admin_model');
				$this->load->view('Admin/Link/csslink');
				$this->load->view('Admin/sidebar');		
				$msg = array('upload_data' => $this->upload->data());
				$this->load->view('Admin/upload_image', $msg);
				$this->load->view('Admin/footer');		   
				$this->load->view('Admin/Link/jslink');
			}
        }*/
		
		public function do_upload()
        {
		   if(!isset( $_SESSION['logged_in']))
			{
				session_destroy();
				redirect('welcome/login', 'refresh');
			} else{
					 $filename = time();
					 $config['allowed_types']        = 'gif|jpg|png';
					 $config['upload_path']          = 'assets/';
					 $config['file_name'] 			 = $filename.'.jpg';
					 $this->load->library('upload', $config); 
			}
			
			if ( ! $this->upload->do_upload('upload_image')) 
			{
				   return FALSE;
		    } else { 
					$tableData = array('file_name'=>$filename,
					  'created_at'=>date('Y-m-d H:i:s'),						
					  'status'=>1);
					  $this->load->model('admin_model');
					  $id = $this->admin_model->insertTable('file_upload', $tableData);		
						
		   			 redirect('upload/viewUploadedFile');
		    }
		}
		
		public function viewUploadedFile()
		{	
			$data = new stdClass();
			$id = new stdClass();
			$this->load->model('admin_model');
			$data->fileList = $this->admin_model->getTableData('file_upload', $id);		
			$this->load->view('Admin/Link/csslink');
			$this->load->view('Admin/sidebar');		
			$this->load->view('Admin/view-uploaded-file', $data);
			$this->load->view('Admin/footer');		   
			$this->load->view('Admin/Link/jslink');
		}
		
		public function deleteFile()
		{
			$this->load->model('admin_model');
			if ($this->admin_model->updateFile($_GET['fileId'], array('status'=>2)))
			{
				$error='updated';
				redirect('upload/viewUploadedFile');				       
			}
		}
		
}
?>