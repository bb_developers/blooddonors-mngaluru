<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
	  {
		  parent::__construct();
		  
		 	$this->load->library(array('session'));
	        $this->load->helper('url');
			$this->load->model('Admin_model');
			$this->load->helper('form');
		   $this->load->library('form_validation');
		
			
	     
	  }
	  
	  public function index()
	    {
		   $this->load->view('Admin/Link/csslink');
		   $this->load->view('Admin/Link/head');
		   $this->load->view('Admin/index');
		   $this->load->view('Admin/Link/jslink');
  
		}
	  public function get_registers()
	    {
		 $data['users']=$this->Admin_model->get_registers();
		 $this->load->view('Admin/Link/csslink');
		 $this->load->view('Admin/Link/head');
    	 $this->load->view('Admin/Registers_details', $data);
		 $this->load->view('Admin/Link/jstablelink');
		}	
	 public function update_news()
	  {
	      $this->load->view('Admin/Link/csslink');
		   $this->load->view('Admin/Link/head');
		   $this->load->view('Admin/update_news');
		   $this->load->view('Admin/Link/jslink');
		   
	  }	
	 public function send_sms()
	  {
	       $this->load->view('Admin/Link/csslink');
		   $this->load->view('Admin/Link/head');
		   $this->load->view('Admin/Smspage');
		   $this->load->view('Admin/Link/jslink');
		   
	  } 
	   public function view_sms()
	    {
		 $data['sms']=$this->Admin_model->get_sms();
		 $this->load->view('Admin/Link/csslink');
		 $this->load->view('Admin/Link/head');
    	 $this->load->view('Admin/sms_views', $data);
		 $this->load->view('Admin/Link/jstablelink');
		}	
		public function view_news()
	    {
		 $data['news']=$this->Admin_model->get_news();
		 $this->load->view('Admin/Link/csslink');
		 $this->load->view('Admin/Link/head');
    	 $this->load->view('Admin/news_view', $data);
		 $this->load->view('Admin/Link/jstablelink');
		}	
	 public function server_sending()
	  {
	    if(!isset( $_SESSION['logged_in']))
			{
				session_destroy();
				redirect('welcome/login', 'refresh');
			} else{
			
			$title=$this->input->post('title');
			$message=$this->input->post('description');
			 $tableData = array(
					'title'=>$title,
					'message'=>$message,
					'dates'=>date('Y-m-d H:i:s'));
			if($this->Admin_model->save_sms_data($tableData))
			 {
			 			   	redirect('Dashboard', 'refresh');

			    } 
	  
	      } 
	  }
	public function do_upload()
        {
		   if(!isset( $_SESSION['logged_in']))
			{
				session_destroy();
				redirect('welcome/login', 'refresh');
			} else{
					 $filename = time();
					 $config['upload_path'] = 'assets/Admin/images/';
					 $config['allowed_types']= 'gif|jpg|png';
					 $config['file_name'] = $filename.'.jpg';
					 $this->load->library('upload', $config);
			
			if (!$this->upload->do_upload('file')) 
			{
				   return FALSE;
		    } else { 
			   
					$tableData = array(
					'title'=>$this->input->post('title'),
					'description'=>$this->input->post('description'),
					'img'=>$config['file_name'],
					  'dates'=>date('Y-m-d H:i:s'));
					  $id = $this->Admin_model->create_news($tableData);		
		   	  		        redirect('Dashboard');
		      }
			}
		}
		
	
		
		
		
		public function deleteFile()
		{
			$this->load->model('admin_model');
			if ($this->admin_model->updateFile($_GET['fileId'], array('status'=>2)))
			{
				$error='updated';
				redirect('upload/viewUploadedFile');				       
			}
		}
	   
	
}
