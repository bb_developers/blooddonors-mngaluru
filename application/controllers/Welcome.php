<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
	  {
		  parent::__construct();
		  
		 	$this->load->library(array('session'));
	        $this->load->helper('url');
	     
	  }
	  
	  public function index()
	    {
		   $this->login(); 
		}
	  
	  
	  public function Myprofile() {
		
		// create the data object
		$data=array();
		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
	    $this->load->model('user_model');
		$action=$this->input->get('action');
		$action_id=$this->input->get('articleID');
	    $this->load->helper('security');
		
		   $_SESSION['action_id']=$action_id;
				    if(isset($action_id) && $action=='edit')
				        {
					        $this->load->model('abbakkanews');
					        $data1['menu']=$this->abbakkanews->get_menu();
					        $data['profile']=$this->user_model->get_profile($action_id);
	                        $this->load->view('admin/csslink');
				            $this->load->view('admin/ad_header', $data1);
					        $this->load->view('admin/create_program/myprofile', $data);
					        $this->load->view('admin/jslink');
					    }
						   
						elseif(isset($action_id) && $action=='delete')
				           {
						           $id=$this->user_model->delete_profile($action_id);
					               $data['error']='Your news ID '.' '.$id.' '.' record deleted parmanently';
							       $this->load->model('abbakkanews');
					               $data1['menu']=$this->abbakkanews->get_menu();
					                $data['profiles']=$this->user_model->get_profiles();
                                   $this->load->view('admin/csslink');
				                   $this->load->view('admin/ad_header', $data1);
					               $this->load->view('admin/create_program/myprofile', $data);
					               $this->load->view('admin/jslink');
				           }
					       
						else
						   {
						   $this->load->helper('security');
		// set validation rules
		  $this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[4]|is_unique[mster_user.username]', array(  'is_unique' => 'This username already exists. Please choose another one.'));
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[mster_user.email]');
		$this->form_validation->set_rules('mobile', 'Mobile', 'trim|required|max_length[10]|min_length[10]');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('password_confirm', 'Confirm Password', 'trim|required|min_length[6]|matches[password]');
		
		if ($this->form_validation->run() === false) {
			
			// validation not ok, send validation errors to the view
			
			
			
			
		} else {
			
			// set variables from the form
			$name=$this->input->post('name');
			$username = mysql_real_escape_string($_POST['username']);
			$password = mysql_real_escape_string($_POST['password']);
			$email    = mysql_real_escape_string($_POST['email']);
			$mobile = $this->input->post('mobile');
			$address= $this->input->post('address');
		
			
			if ($this->user_model->create_user($name, $username, $password, $email, $mobile, $address)) {
				
			                	$data['error']='User Created Secussfully';
				                 
				
			} else {
				
				// user creation failed, this should never happen
				$data->error = 'There was a problem creating your new account. Please try again.';
				redirect('welcome/myprofile','refresh');
				
				// send error to the view
				
						            }
					  }
					  
					               $this->load->model('abbakkanews');
					               $data1['menu']=$this->abbakkanews->get_menu();
					               $data['profiles']=$this->user_model->get_profiles();
                                   $this->load->view('admin/csslink');
				                   $this->load->view('admin/ad_header', $data1);
					               $this->load->view('admin/create_program/myprofile', $data);
					               $this->load->view('admin/jslink');
	   }
		
	}
	
	
	
	
	 public function reset_password()	  
			     {
				         $this->load->helper('form');
		                 $this->load->library('form_validation');
			             $this->load->model('user_model');
					     $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		                 $this->form_validation->set_rules('password_confirm', 'Confirm Password', 'trim|required|min_length[6]|matches[password]');
		
					 // $this->form_validation->set_rules('status', 'status', 'trim|required');
						   if ($this->form_validation->run() === false)
			                    {
								$data['error'] = 'error';
							     redirect('welcome/myprofile', 'refresh');
			                	// validation not ok, send validation errors to the view
				                    
			                 	// $link=$this->errorfollow($data);
			             	 
			    	
				                }
		                        else 
			                    {		 
			       			     $password = $this->input->post('password');
								
								   $action_id= $_SESSION['action_id'];
								    if($this->user_model->reset_password($action_id, $password))
									      { 
										   $data['error'] = 'password update successfully Done';
										     redirect('welcome/myprofile', 'refresh');
										  }
								      else 
									      { 
										   $data['error'] = 'Place Insert Database Or Query ERROR';
										   redirect('welcome/myprofile', 'refresh');
									      }		  
				                 }
				  			 
					
					 
			   }// method end
			
	
	  public function login()
	       {
		     $data = new stdClass();
		
		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->run();
		   $this->load->model('user_model');
			$this->form_validation->set_rules('username','username','required');
			 $this->form_validation->set_rules('password','password','required');
			
			   if($this->form_validation->run() == false)
		       	{
				      $data->error='please enter the username and password';
		              $this->load->view('Admin/Link/csslink');
					  $this->load->view('login', $data);
		              $this->load->view('Admin/Link/jslink');
					
		        }
			  else
			    {
			      $username= $this->input->post('username');
				  $password=$this->input->post('password');
					
					 if($this->user_model->user_login($username, $password))
					    {
						$user_id=$this->user_model->get_user_id_from_username($username);
						//sesssion assign
						$_SESSION['username']=(string)$username;
						$_SESSION['logged_in']=(bool)true;
						$_SESSION['status']='log_in';
						 		      redirect(base_url().'Dashboard');				
						}
						
					else
					   {
					   $data->error='wrong username or password.';
		               $this->load->view('Admin/Link/csslink');
					   $this->load->view('login', $data);
		               $this->load->view('Admin/Link/jslink');
					   }
					
								
					
		         }
	  
		   }
		
	
}
